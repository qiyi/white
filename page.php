<?php get_header(); ?>

<div id="entry">
<?php if(have_posts()): ?> <?php while(have_posts()): the_post(); ?>
	<div id="post-<?php the_ID(); ?>" class="post-page">
		<h3 class="page-title"><?php the_title(); ?></h3>
		<div class="content">
			<?php the_content('&raquo;继续阅读'); ?>
			<?php link_pages('<p><strong>Pages:</strong>','</p>','number'); ?>
		</div>
		<div id="commenta">
			<?php comments_template('', true); ?>
		</div>
	</div>
<?php endwhile; ?>
<?php else: ?>
	<div class="post" id="post-<?php the_ID(); ?>">
		<h2><?php _e('Not Found'); ?></h2>
	</div>
<?php endif; ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>