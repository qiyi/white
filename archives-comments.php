<?php
/*
Template Name: Comments
*/
?>
<?php get_header(); ?>
<div id="entry">
	<div class="post-page" id="archives-comments">
	<h3 class="page-title">评论 Comments</h3>
	<div class="content">
<?php 
$sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_author_email, comment_author_url, comment_date, comment_approved, comment_type, comment_content FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date DESC LIMIT 30";
$comments = $wpdb->get_results($sql);
$count=0;
foreach ($comments as $comment) { 
?>
<div class="comment nova" id="comment-<?php comment_ID() ?>">
<div class="gravatar alignleft"><?php echo get_avatar( $comment, 40, 'http://images.isouth.org/qi/guest.gif' ); ?></div>
<div class="comment-content"><strong><?php comment_author_link() ?></strong><br/>
<small class="commentmetadata"><a href="<?php echo get_permalink($comment->ID);?>#comment-<?php comment_ID() ?>" title="评论">#<?php printf('%1$s', ++$count); ?></a> <?php comment_date('Y年m月d日 l') ?> <?php comment_time() ?> <?php edit_comment_link('编辑','[ ',' ]'); ?> [ <a href="<?php echo get_permalink($comment->ID);?>#respond" >回复</a> ] [ <a href="<?php echo get_permalink($comment->ID);?>" rel="bookmark" title="到《<?php echo $comment->post_title;?>》的永久链接">原文链接</a> ]</small>
<?php comment_text() ?></div>
</div>
<?php 
}
?>
	</div>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>