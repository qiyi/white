</div>
<div id="footer">
<div class="yi">
<div class="footinfo">
themed by <a href="https://bitbucket.org/qiyi/white" rel="external" style="color:#FF0000;">white</a> <span style="color:#999999;">1.18</span>&nbsp;&nbsp;hosted by <a href="http://www.hengtian.org/" style="color:#FF6600;" rel="external nofollow">衡天主机</a>&nbsp;&nbsp;copyright &#169; 2025 <a href="http://isouth.org/" rel="home me license" style="color: #37AB22;">isouth.org</a>&nbsp;&nbsp;<?php echo get_num_queries(); ?> queries in <?php timer_stop(1); ?> seconds.
</div>
<div class="gotop"><a href="#top" onclick="goTop();return false;">回到顶端&nbsp;&uarr;</a></div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<?php if ( is_single() or is_page('92') ){ ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri() . "/js/comments-ajax.js" ?>"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri() . "/js/custom.js" ?>"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-9059734-3', 'auto');
  ga('send', 'pageview');
</script>
</div>
<?php wp_footer(); ?>
</body>
</html>
