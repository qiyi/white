<!DOCTYPE html>
<html>
<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <title><?php 
if ( is_single() || is_page() || is_category() || is_tag() ) {
    wp_title('');
} else { 
    bloginfo('name');
}
if ( $paged == "" ) { 
    $pagenum = "";
} else { 
    echo $pagenum = "- 第".$paged." 页"; 
}
?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11" />
<?php
if (is_home()) {
    $description = get_bloginfo('description');
} elseif ( is_single() ) {
    if ($post->post_excerpt) {
        $description = $post->post_excerpt;
    } else {
        $description = mb_substr( strip_tags( $post->post_content ), 0, 220, "UTF-8" );
    }
} elseif (is_category() ) {
    $description = category_description();
} ?>  <meta name="description" content="<?=$description?>" />
  <link rel="shortcut icon" href="/fav.ico"  type="image/x-icon" />
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />
  <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
  <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name');; ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <?php wp_head(); ?>
</head> 
<body>
<div id="header">
  <div class="logo">  
      <a href="<?php bloginfo('url'); ?>" class="name" title="<?php bloginfo('name'); ?>" rel="home me"><?php bloginfo('name'); ?></a>
  </div>
  <div id="search">
    <?php include(TEMPLATEPATH .'/searchform.php'); ?>
  </div>
</div>
<div id="wrapper" class="clearfix">
