<?php
/*
Template Name: webslice
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<title>iSouth Web Slice</title> 
<style type="text/css">
body,div,p,ul{margin:0;padding:0;}
body{overflow:auto;margin-left:5px;}
a,a:visited{color:#379BCD;text-decoration:none;}
a:hover{color:#000;text-decoration:underline;}
.entry-content{position:relative;width:300px;font:12px/160% \5FAE\8F6F\96C5\9ED1,\5B8B\4F53,Helvetica,Tahoma,Arial;color:#333;}
ul{margin:15px 0 10px 0;}
ul li{padding:5px 0;list-style:disc inside;color:#379BCD;}
.qi{padding:10px;}
.title{color:#379BCD;font-size:14px;}
.logo{float:right;font-size:16px;}
.footer a{color:#666;text-decoration:underline;}
</style>
<base target="_blank"/> 
</head> 
<body> 
	<div class="qi hslice" id="webslice-post">
		<p class="entry-title" style="display:none;"><?php bloginfo('name'); ?>最新文章</p>
		<a rel="entry-content" href="webslice" style="display:none;"></a>
		<a rel="bookmark" href="<?php bloginfo('url'); ?>" style="display:none;"></a>
		<p class="ttl" style="display:none;">60</p>
		<div class="entry-content">
		<div class="logo"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></div> 
		<div class="title">最新文章</div> 
		<ul><?php wp_get_archives( array( 'type' => 'postbypost', 'limit' => 5 ) ); ?></ul>
		<p class="footer">&copy;2015 <a href="<?php bloginfo('url'); ?>">iSouth.org</a> | <a href="<?php bloginfo('url'); ?>">查看全部文章</a></p>
		</div>
	</div>
</body> 
</html>
