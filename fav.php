<?php
$permalink = urlencode(get_permalink($post->ID));
$title = urlencode($post->post_title);
$title = str_replace('+','%20',$title);
?>
<div class="fav">收藏与分享 : 
<a href="https://twitter.com/intent/tweet?url=<?php echo $permalink; ?>&amp;text=<?php echo $title; ?>" rel="external nofollow" title="分享到 Twitter "> Twitter</a> | 
<a href="http://www.facebook.com/share.php?u=<?php echo $permalink; ?>" rel="external nofollow" title="分享到 Facebook">Facebook</a> | 
<a href="http://v.t.sina.com.cn/share/share.php?title=<?php echo $title; ?>&amp;url=<?php echo $permalink; ?>&amp;source=isouth.org&amp;sourceUrl=<?php echo $permalink; ?>" rel="external nofollow" title="分享到 微博">微博</a> | 
<a href="http://share.renren.com/share/buttonshare.do?link=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>" rel="external nofollow" title="分享到 人人">人人</a> | 
<a href="https://plus.google.com/share?url=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>&amp;srcURL=<?php echo urlencode(get_option('home'));?>" rel="external nofollow" title="添加到 Google+">Google+</a> | 
<a href="http://pdfmyurl.com?url=<?php echo $permalink; ?>" rel="external nofollow" title="另存为 PDF 格式">PDF</a>
</div>