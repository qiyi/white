<?php get_header(); ?>
<div id="entry">
<?php
if(have_posts()) {
	while(have_posts()) { 
		the_post();
?><div id="post-<?php the_ID(); ?>" class="post">
	<h1 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a></h1>
	<div class="postmeta">
	<?php the_time('Y/m/d G:H'); ?><?php _e(' - Posted By '); ?><?php the_author_posts_link(); ?><?php _e(' - Tags&#58;'); ?> <?php the_tags('', ',', ' '); ?><?php _e(' - Category&#58;'); ?> <?php the_category(',') ?> 
	</div>
	<div class="post-comment">
	<?php comments_popup_link('<span class="large">0</span> 条评论', '<span class="large">1</span> 条评论', '<span class="large">%</span> 条评论', '', 'None'); ?>
	</div>
	<div class="content">
		<?php the_content(); ?>
		<?php link_pages('<p><strong>Pages:</strong>','</p>','number'); ?>
		<?php include (TEMPLATEPATH . '/fav.php'); ?> 
	</div>
	<div id="commenta">
		<?php comments_template('', true); ?>
	</div>
	</div>
<?php
	}
} else {
?><div class="post">
		<h2><?php _e('Not Found'); ?></h2>
	</div>
<?php
} 
?>
</div>
<?php get_footer(); ?>