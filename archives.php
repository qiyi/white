<?php
/*
Template Name: Archives
*/
?>
<?php get_header(); ?>
<div id="entry">
	<div class="post-page">
			<h3 class="page-title">存档 Archives</h3>
                <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                <option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option> 
                <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
                </select>
			<h3 class="page-title">所有标签 TagCloud</h3>
			<div><?php wp_tag_cloud('smallest=9&largest=18&order=RAND'); ?></div>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>