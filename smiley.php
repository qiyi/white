<script type="text/javascript">
/* <![CDATA[ */
    function grin(tag) {
    	var myField;
    	tag = ' ' + tag + ' ';
        if (document.getElementById('comment') && document.getElementById('comment').type == 'textarea') {
    		myField = document.getElementById('comment');
    	} else {
    		return false;
    	}
    	if (document.selection) {
    		myField.focus();
    		sel = document.selection.createRange();
    		sel.text = tag;
    		myField.focus();
    	}
    	else if (myField.selectionStart || myField.selectionStart == '0') {
    		var startPos = myField.selectionStart;
    		var endPos = myField.selectionEnd;
    		var cursorPos = endPos;
    		myField.value = myField.value.substring(0, startPos)
    					  + tag
    					  + myField.value.substring(endPos, myField.value.length);
    		cursorPos += tag.length;
    		myField.focus();
    		myField.selectionStart = cursorPos;
    		myField.selectionEnd = cursorPos;
    	}
    	else {
    		myField.value += tag;
    		myField.focus();
    	}
    }
/* ]]> */
</script>
<p>
<a href="javascript:grin(':smile:')" title="微笑"><img src="http://images.isouth.org/emoticons/icon_smile.gif" alt="" /></a>
<a href="javascript:grin(':???:')" title="发呆"><img src="http://images.isouth.org/emoticons/icon_confused.gif" alt="" /></a>
<a href="javascript:grin(':cry:')" title="流泪"><img src="http://images.isouth.org/emoticons/icon_cry.gif" alt="" /></a>
<a href="javascript:grin(':oops:')" title="害羞"><img src="http://images.isouth.org/emoticons/icon_redface.gif" alt="" /></a>
<a href="javascript:grin(':mad:')" title="发怒"><img src="http://images.isouth.org/emoticons/icon_mad.gif" alt="" /></a>
<a href="javascript:grin(':razz:')" title="调皮"><img src="http://images.isouth.org/emoticons/icon_razz.gif" alt="" /></a>
<a href="javascript:grin(':grin:')" title="龇牙"><img src="http://images.isouth.org/emoticons/icon_biggrin.gif" alt="" /></a>
<a href="javascript:grin(':eek:')" title="惊讶"><img src="http://images.isouth.org/emoticons/icon_surprised.gif" alt="" /></a>
<a href="javascript:grin(':cool:')" title="酷"><img src="http://images.isouth.org/emoticons/icon_cool.gif" alt="" /></a>
<a href="javascript:grin(':!:')" title="抓狂"><img src="http://images.isouth.org/emoticons/icon_exclaim.gif" alt="" /></a>
<a href="javascript:grin(':wink:')" title="可爱"><img src="http://images.isouth.org/emoticons/icon_wink.gif" alt="" /></a>
<a href="javascript:grin(':shock:')" title="惊恐"><img src="http://images.isouth.org/emoticons/icon_eek.gif" alt="" /></a>
<a href="javascript:grin(':neutral:')" title="流汗"><img src="http://images.isouth.org/emoticons/icon_neutral.gif" alt="" /></a>
<a href="javascript:grin(':lol:')" title="憨笑"><img src="http://images.isouth.org/emoticons/icon_lol.gif" alt="" /></a>
<a href="javascript:grin(':?:')" title="疑问"><img src="http://images.isouth.org/emoticons/icon_question.gif" alt="" /></a>
<a href="javascript:grin(':roll:')" title="晕"><img src="http://images.isouth.org/emoticons/icon_rolleyes.gif" alt="" /></a>
<a href="javascript:grin(':idea:')" title="衰"><img src="http://images.isouth.org/emoticons/icon_idea.gif" alt="" /></a>
<a href="javascript:grin(':arrow:')" title="抠鼻"><img src="http://images.isouth.org/emoticons/icon_arrow.gif" alt="" /></a>
<a href="javascript:grin(':evil:')" title="坏笑"><img src="http://images.isouth.org/emoticons/icon_evil.gif" alt="" /></a>
<a href="javascript:grin(':mrgreen:')" title="鄙视"><img src="http://images.isouth.org/emoticons/icon_mrgreen.gif" alt="" /></a>
<a href="javascript:grin(':sad:')" title="快哭了"><img src="http://images.isouth.org/emoticons/icon_sad.gif" alt="" /></a>
<a href="javascript:grin(':twisted:')" title="阴险"><img src="http://images.isouth.org/emoticons/icon_twisted.gif" alt="" /></a>
</p>