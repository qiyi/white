<?php get_header(); ?>
<div id="entry">
<?php if(have_posts()): ?> <?php while(have_posts()): the_post(); ?>
	<div id="post-<?php the_ID(); ?>" class="post">
	<h2 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span class="post-title"><?php the_title(); ?></span></a></h2>
	<div class="postmeta">
		<?php the_time('Y/m/d'); ?><?php _e(' - Posted By '); ?><?php the_author_posts_link(); ?>
	</div>
	<div class="post-comment">
	<?php comments_popup_link('<span class="large">0</span> 条评论', '<span class="large">1</span> 条评论', '<span class="large">%</span> 条评论', '', 'None'); ?>
	</div>
	<div class="content"> 
		<?php the_content(__('&raquo;阅读全文')); ?>
		<div class="postmeta">
			<?php _e('Tags&#58;'); ?> <?php the_tags('', ',', ' '); ?><?php _e(' - Category&#58;'); ?> <?php the_category(',') ?> 
		</div>
	</div>
	</div>
<?php endwhile; ?>
<div class="page_navi"><?php par_pagenavi(9); ?></div>
<?php else: ?>
	<div class="post" id="post-<?php the_ID(); ?>">
	<h2><?php _e('Not Found'); ?></h2>
	</div>
<?php endif; ?>
</div>
<?php get_footer(); ?>