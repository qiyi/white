<?php get_header(); ?>
<div id="entry">
	<h3 class="page-title">存档 Archives :<?php wp_title(''); ?></h3>
	<?php if(have_posts()): ?><?php while(have_posts()): the_post(); ?>
	<div class="post" id="post-<?php the_ID(); ?>">
	<h2 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span class="post-title"><?php the_title(); ?></span></a></h2>
	<div class="postmeta">
	<?php the_time('Y/m/d'); ?><?php _e(' - Posted By '); ?><?php the_author_posts_link(); ?>
	</div>
	<div class="post-comment">
	<?php comments_popup_link('<span class="large">0</span> 条评论', '<span class="large">1</span> 条评论', '<span class="large">%</span> 条评论', '', 'None'); ?> <?php edit_post_link(' Edit',' &#124;',''); ?>
	</div>
	</div>
	<?php endwhile; ?>
	<div class="page_navi"><?php par_pagenavi(9); ?></div>
	<?php else: ?>
	<div id="post-<?php the_ID(); ?>" class="post" >
	<h3>好像出了一点小状况</h3>
	<p>您所访问的这个页面好像不存在了或者还没有这个页面，你可以搜索试试，或者去其他页面看看。</p>
	</div>
<?php endif; ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>