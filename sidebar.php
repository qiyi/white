<div id="sidebar">
<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar() ) ; else ; ?>
	<div class="widget">
	<div class="sidebar-feature sidebar-feeds"><a href="http://isouth.org/feed/" rel="external nofollow" title="订阅 iSouth 的 RSS Feed">订阅 RSS Feed</a></div>
	</div>
	<div class="widget">
	<div class="sidebar-feature sidebar-twitter"><a href="https://twitter.com/xuqingyi" rel="external nofollow" title="Follow @qiyi on Twitter">Follow @qiyi on Twitter</a></div>
	</div>
	<div class="widget hslice" id="webslice">
		<p class="entry-title hdn">iSouth 最新文章</p>
		<a rel="entry-content" href="<?php bloginfo('url'); ?>/webslice" class="hdn"></a>
		<a rel="bookmark" href="<?php bloginfo('url'); ?>" class="hdn"></a>
		<p class="ttl hdn">60</p>
		<h4><span class="sidebar-title">最新文章 <a onclick="window.external.AddToFavoritesBar('<?php bloginfo('url'); ?>/#webslice','iSouth Web Slice','slice');" style="cursor:pointer;color:#37AB22;" class="sidebar-more" title="订阅 iSouth 最新文章的 Web Slice">WebSlice</a></span></h4>
		<ul class="entry-content"><?php wp_get_archives('type=postbypost&limit=7'); ?></ul>
	</div>
	<?php if ( is_home() or is_page () ) { ?>
    <div class="widget">
		<h4><span class="sidebar-title">最新评论 <a href="<?php bloginfo('url'); ?>/comments/" class="sidebar-more" title="查看 iSouth 最近的 30 条评论">Comment</a></span></h4>
		<ul>
<?php
global $wpdb;
$sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID,
comment_post_ID, comment_author, comment_date_gmt, comment_approved,
comment_type,comment_author_url,
comment_content AS com_excerpt
FROM $wpdb->comments
LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID =
$wpdb->posts.ID)
WHERE comment_approved = '1' AND comment_type = '' AND comment_author != '起衣' AND
post_password = ''
ORDER BY comment_date_gmt DESC
LIMIT 7";
$comments = $wpdb->get_results($sql);
$output = "";
foreach ($comments as $comment) {
$output .= "\n<li>"." <a href=\"" . get_permalink($comment->ID) .
"#comment-" . $comment->comment_ID . "\" title=\"" .
$comment->post_title . "\" rel=\"nofollow\">" .strip_tags($comment->comment_author)
."："  . cut_str(strip_tags($comment->com_excerpt),10)
."</a></li>";
}
echo $output;?>
		</ul>
	</div>
	<div class="widget spalten">
		<h4><span class="sidebar-title">文章归档 <a href="<?php bloginfo('url'); ?>/archive/" class="sidebar-more" title="查看 iSouth 所有文章的归档">Archive</a></span></h4>
		<ul class="clearfix">
			<?php wp_get_archives('type=monthly&limit=14'); ?>
		</ul>
	</div>
	<div class="widget spalten">
		<h4><span class="sidebar-title">友情链接 <a href="<?php bloginfo('url'); ?>/blogroll/" class="sidebar-more" title="查看 iSouth 所有的友情链接">BlogRoll</a></span></h4>
		<ul class="clearfix">
			<?php wp_list_bookmarks('categorize=0&title_li=&orderby=rand&title_before=<li>&title_after=</li>'); ?>
		</ul>
	</div>
	<div class="widget">
		<h4><span class="sidebar-title">标签云 <span class="sidebar-more">TagCloud</span></span></h4>
		<div><?php wp_tag_cloud('smallest=10&largest=16&order=RAND'); ?></div>
	</div>
	<?php } ?>
	<div class="widget">
		<h4><span class="sidebar-title">管理功能 <span class="sidebar-more">Admin</span></span></h4>
		<ul>
			<li><a href="<?php bloginfo('url'); ?>/contact/">About</a></li>
			<!--<li><a href="http://m.isouth.org/">Mobile</a></li>-->
			<li><a href="http://wiki.isouth.org/" rel="external">Wiki</a></li>
			<?php wp_register(); ?>
			<li><?php wp_loginout(); ?></li>
			
		</ul>
	</div>
</div>
