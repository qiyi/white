﻿jQuery(document).ready(function($){
//嵌套的reply按钮
$('.commentlist li').each(function(){
var re = $(this).find('ul.children li .reply-button');
$(this).find(".reply:first").clone().appendTo(re);
});
$('.reply').click(function(){
var rename = $(this).parents(".comments").find(".comment-author").text();
var reid = '"#div-comment-' + $(this).parents(".comments").find("cite").text() + '"';
$("#comment").append("&lt;a href=" + reid + "&gt;@" + rename +"&lt;/a&gt;&nbsp;").focus();
});
$('#cancel-comment-reply').click(function() {
$("#comment").empty();
});
$("a[rel='external'],a[rel='external nofollow']").click(function(){window.open(this.href);return false;});
});
//Search
(function() { var f = document.getElementById('cse-search-box'); if (!f) { f = document.getElementById('searchbox_demo'); } if (f && f.q) { var q = f.q; var n = navigator; var l = location; var su = function () { var u = document.createElement('input'); var v = document.location.toString(); var existingSiteurl = /(?:[?&]siteurl=)([^&#]*)/.exec(v); if (existingSiteurl) { v = decodeURI(existingSiteurl[1]); } var delimIndex = v.indexOf('://'); if (delimIndex >= 0) { v = v.substring(delimIndex + '://'.length, v.length); } u.name = 'siteurl'; u.value = v; u.type = 'hidden'; f.appendChild(u); }; if (n.appName == 'Microsoft Internet Explorer') { var s = f.parentNode.childNodes; for (var i = 0; i < s.length; i++) { if (s[i].nodeName == 'SCRIPT' && s[i].attributes['src'] && s[i].attributes['src'].nodeValue == unescape('http:\x2F\x2Fwww.google.com\x2Fcse\x2Fbrand?form=cse-search-box\x26lang=zh-Hans')) { su(); break; } } } else { su(); }  if (window.history.navigationMode) { window.history.navigationMode = 'compatible'; } var b = function() { if (q.value == '') { q.value= '输入关键词'; } }; var f = function() { if(q.value== '输入关键词') q.value = ''; }; q.onfocus = f; q.onblur = b; if (!/[&?]q=[^&]/.test(l.search)) { b(); } } })();
//goTop
function goTop(acceleration, time) {
	acceleration = acceleration || 0.1;
	time = time || 16;
	var x1 = 0;
	var y1 = 0;
	var x2 = 0;
	var y2 = 0;
	var x3 = 0;
	var y3 = 0;
	if (document.documentElement) {
		x1 = document.documentElement.scrollLeft || 0;
		y1 = document.documentElement.scrollTop || 0;
	}
	if (document.body) {
		x2 = document.body.scrollLeft || 0;
		y2 = document.body.scrollTop || 0;
	}
	var x3 = window.scrollX || 0;
	var y3 = window.scrollY || 0;
	var x = Math.max(x1, Math.max(x2, x3));
	var y = Math.max(y1, Math.max(y2, y3));
	var speed = 1 + acceleration;
	window.scrollTo(Math.floor(x / speed), Math.floor(y / speed));
	if(x > 0 || y > 0) {
		var invokeFunction = "goTop(" + acceleration + ", " + time + ")";
		window.setTimeout(invokeFunction, time);
	}
}