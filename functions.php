<?php
/* comment_mail_notify v1.0 beta by willin kan. (所有回复都发邮件) */
function comment_mail_notify($comment_id) {
    $comment = get_comment($comment_id);
    $comment_author = trim($comment->comment_author);
    $parent_id = $comment->comment_parent;
    $newContent = explode("#div-comment-", $comment->comment_content);
    $newContent1 = explode("\"", $newContent[1]); 
    if(ereg('^[0-9]+$',$newContent1[0])){
        $parent_id=$newContent1[0];
    }
    $spam_confirmed = $comment->comment_approved;
    if (($parent_id != '') && ($spam_confirmed != 'spam') && ($parent_id != 0) ) {
        $wp_email = 'no-reply@' . preg_replace('#^www\.#', '', strtolower($_SERVER['SERVER_NAME'])); //e-mail 发出点, no-reply 可改为可用的 e-mail.
        $to = trim(get_comment($parent_id)->comment_author_email);
        $subject = '您在 [' . get_option("blogname") . '] 的留言有新的回复';
        $message = '
<div style="background-color:#eef2fa; border:1px solid #d8e3e8; color:#111; padding:0 15px; -moz-border-radius:5px; -webkit-border-radius:5px; -khtml-border-radius:5px;">
<p>' . trim(get_comment($parent_id)->comment_author) . ',您好!</p>
<p>您曾在《' . get_the_title($comment->comment_post_ID) . '》发表的留言:</p><div style="padding:5px;border:1px solid #888;">' . trim(get_comment($parent_id)->comment_content) . '
</div><p>' . $comment_author . '给了您新的回复:</p><div style="padding:5px;border:1px solid #888;">' . trim($comment->comment_content) . '
</div><p>您可以点击这里 <a href="' . htmlspecialchars(get_comment_link($parent_id, array('type' => comment))) . '">查看回复的完整内容</a></p>
<p><b>感谢您对 <a href="' . get_option('home') . '">' . get_option('blogname') . '</a> 的关注，欢迎<a href="http://feeds.feedburner.com/isouth/">订阅本站</a></b></p>
<p>(此邮件由系统自动发出，请勿回复)</p>
</div>';
        $message = convert_smilies($message);
        $from = "From: \"" . get_option('blogname') . "\" <$wp_email>";
        $headers = "$from\nContent-Type: text/html; charset=" . get_option('blog_charset') . "\n";
        wp_mail( $to, $subject, $message, $headers );
    }
}
add_action('comment_post', 'comment_mail_notify');

/*禁用全角自动转换*/
remove_filter('the_content', 'wptexturize');

/*分页导航*/
function par_pagenavi($range = 9){
    global $paged, $wp_query;
    if(!isset($max_page)){
        $max_page = $wp_query->max_num_pages;
    }
    if($max_page > 1){
        if(!$paged){
            $paged = 1;
        }
        if($paged != 1){
            echo "<a href='" . get_pagenum_link(1) . "' class='extend' title='跳转到首页'> 返回首页 </a>";
        }
        previous_posts_link(' 上一页 ');
        if($max_page > $range){
            if($paged < $range){
                for($i = 1; $i <= ($range + 1); $i++){
                    echo "<a href='" . get_pagenum_link($i) ."' rel='nofollow'";
                    if($i==$paged) {
                        echo " class='current'";
                    }
                    echo ">$i</a>";
                }
            } elseif($paged >= ($max_page - ceil(($range/2)))){
                for($i = $max_page - $range; $i <= $max_page; $i++){
                    echo "<a href='" . get_pagenum_link($i) ."' rel='nofollow'";
                    if($i==$paged) {
                        echo " class='current'";
                    }
                    echo ">$i</a>";
                }
            } elseif($paged >= $range && $paged < ($max_page - ceil(($range/2)))){
                for($i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++){
                    echo "<a href='" . get_pagenum_link($i) ."' rel='nofollow'";
                    if($i==$paged) {
                        echo " class='current'";
                    }
                    echo ">$i</a>";
                }
            }
        } else{
            for($i = 1; $i <= $max_page; $i++){
                echo "<a href='" . get_pagenum_link($i) ."' rel='nofollow'";
                if($i==$paged) {
                    echo " class='current'";
                }
                echo ">$i</a>";
            }
        }
	    next_posts_link(' 下一页 ');
        if($paged != $max_page){
            echo "<a href='" . get_pagenum_link($max_page) . "' class='extend' title='跳转到最后一页' rel='nofollow'> 最后一页 </a>";
        }
    }
}

/*评论截断*/
function cut_str($string, $sublen, $start = 0, $code = 'UTF-8'){
    if($code == 'UTF-8'){
        $pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
        preg_match_all($pa, $string, $t_string);
        if(count($t_string[0]) - $start > $sublen) {
            return join('', array_slice($t_string[0], $start, $sublen))."...";
        }
        return join('', array_slice($t_string[0], $start, $sublen));
    } else{
        $start = $start*2;
        $sublen = $sublen*2;
        $strlen = strlen($string);
        $tmpstr = '';
        for($i=0; $i<$strlen; $i++){
            if($i>=$start && $i<($start+$sublen)){
                if(ord(substr($string, $i, 1))>129) {
                    $tmpstr.= substr($string, $i, 2);
                } else {
                    $tmpstr.= substr($string, $i, 1);
                }
            }
            if(ord(substr($string, $i, 1))>129) {
                $i++;
            }
        }
        if(strlen($tmpstr)<$strlen ) {
            $tmpstr.= "...";
        }
        return $tmpstr;
    }
}

/* 主题嵌套评论 */
function mytheme_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class('clearfix'); ?> id="comment-<?php comment_ID() ?>" >
    <div id="div-comment-<?php comment_ID(); ?>" class="c">
     	<div class="gravatar"><?php echo get_avatar( $comment, $size = '35', 'http://images.isouth.org/qi/guest.gif'); ?></div>
   		<div class="comments">
		<div class="commentmetadata"> <?php printf(__('<span class="comment-author">%s</span>'), get_comment_author_link()) ?>:<?php printf(__(' %1$s at %2$s'), get_comment_date(),  get_comment_time()) ?><cite class="hdn"><?php comment_ID(); ?></cite> <?php edit_comment_link(__('(Edit)'),'  ','') ?>
		</div>
		<div class="reply-button">
		<div class="reply">
		<?php comment_reply_link(array_merge( $args, array('add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>
		</div>
		<?php if ($comment->comment_approved == '0') : ?>
		<em><?php _e('Your comment is awaiting moderation.') ?></em>
		<br />
		<?php endif; ?>
		<?php comment_text() ?>
		</div>
	</div>
<?php
}
function wp_get_related_posts(){
    global $wpdb, $post,$table_prefix;
    if(!$post->ID){return;}
    $now = current_time('mysql', 1);
    $tags = wp_get_post_tags($post->ID);
    $taglist = "'" . $tags[0]->term_id. "'";
    $tagcount = count($tags);
    if ($tagcount > 1) {
        for ($i = 1; $i < $tagcount; $i++) {
            $taglist = $taglist . ", '" . $tags[$i]->term_id . "'";
        }
    }
    $limitclause = "LIMIT 5";
    $q = "SELECT p.ID, p.post_title, p.post_date,  p.comment_count, count(t_r.object_id) as cnt FROM $wpdb->term_taxonomy t_t, $wpdb->term_relationships t_r, $wpdb->posts p WHERE t_t.taxonomy ='post_tag' AND t_t.term_taxonomy_id = t_r.term_taxonomy_id AND t_r.object_id  = p.ID AND (t_t.term_id IN ($taglist)) AND p.ID != $post->ID AND p.post_status = 'publish' AND p.post_date_gmt < '$now' GROUP BY t_r.object_id ORDER BY cnt DESC, p.post_date_gmt DESC $limitclause;";
    $related_posts = $wpdb->get_results($q);
    $output = "";
    if (!$related_posts){$output  .= '<li>无相关日志</li>';}
    foreach ($related_posts as $related_post ){
        $dateformat = get_option('date_format');
        $output .= '<li>';
        $output .=  '<a href="'.get_permalink($related_post->ID).'" title="'.wptexturize($related_post->post_title).' ('.mysql2date($dateformat, $related_post->post_date).')">'.wptexturize($related_post->post_title).'</a>';
        $output .=  '</li>';
    }
    $output = '<p class="copyright">本文链接: <a href="'.get_permalink($post->ID).'" title="'.wptexturize($post->post_title).'" rel="bookmark license"> '.get_permalink($post->ID).'</a> , 转载请注明出处，此外还可以<a href="http://isouth.org/feed/" rel="external nofollow" title="订阅 iSouth 的 RSS Feed">订阅我</a>。</p><h3>相关日志 <small>Relate Posts</small></h3><ul class="related_post">' . $output . '</ul>';
    return $output;
}
function wp_related_posts_attach($content){
    if (is_single()||is_feed()){
      $output = wp_get_related_posts();
      $content = $content . $output;
    }
	return $content;
}
add_filter('the_content', 'wp_related_posts_attach',100);

/* <<小牆>> Anti-Spam v1.82 by Willin Kan. 2010/12/16 最新修改 */
//建立
class anti_spam {
  function anti_spam() {
    if ( !current_user_can('level_0') ) {
      add_action('template_redirect', array($this, 'w_tb'), 1);
      add_action('init', array($this, 'gate'), 1);
      add_action('preprocess_comment', array($this, 'sink'), 1);
    }
  }
  //設欄位
  function w_tb() {
    if ( is_singular() ) {
      ob_start(create_function('$input','return preg_replace("#textarea(.*?)name=([\"\'])comment([\"\'])(.+)/textarea>#",
      "textarea$1name=$2w$3$4/textarea><textarea name=\"comment\" cols=\"100%\" rows=\"4\" style=\"display:none\"></textarea>",$input);') );
    }
  }
  //檢查
  function gate() {
    if ( !empty($_POST['w']) && empty($_POST['comment']) ) {
      $_POST['comment'] = $_POST['w'];
    } else {
      $request = $_SERVER['REQUEST_URI'];
      $referer = isset($_SERVER['HTTP_REFERER'])         ? $_SERVER['HTTP_REFERER']         : '隱瞞';
      $IP      = isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] . ' (透過代理)' : $_SERVER["REMOTE_ADDR"];
      $way     = isset($_POST['w'])                      ? '手動操作'                       : '未經評論表格';
      $spamcom = isset($_POST['comment'])                ? $_POST['comment']                : null;
      $_POST['spam_confirmed'] = "請求: ". $request. "\n來路: ". $referer. "\nIP: ". $IP. "\n方式: ". $way. "\n內容: ". $spamcom. "\n -- 記錄成功 --";
    }
  }
  //處理
  function sink( $comment ) {
    if ( !empty($_POST['spam_confirmed']) ) {
      if ( in_array( $comment['comment_type'], array('pingback', 'trackback') ) ) return $comment; //不管 Trackbacks/Pingbacks
      //方法一: 直接擋掉, 將 die(); 前面兩斜線刪除即可.
      //die();
      //方法二: 標記為 spam, 留在資料庫檢查是否誤判.
      add_filter('pre_comment_approved', create_function('', 'return "spam";'));
      $comment['comment_content'] = "[ 小牆判斷這是Spam! ]\n". $_POST['spam_confirmed'];
    }
    return $comment;
  }
}
$anti_spam = new anti_spam();
// -- END ----------------------------------------

//no self ping
function no_self_ping( &$links ) {
	$home = get_option( 'home' );
	foreach ( $links as $l => $link )
		if ( 0 === strpos( $link, $home ) )
			unset($links[$l]);
}
add_action( 'pre_ping', 'no_self_ping' );

//gravatar缓存
function cache_gravatar($avatar) {
 $tmp = strpos($avatar, 'http'); //76
 $g = substr($avatar, $tmp, strpos($avatar, "'", $tmp) - $tmp);//图像地址
 $tmp = strpos($g, 'avatar/') + 7;//31
 $f = substr($g, $tmp, strpos($g, "?", $tmp) - $tmp);//图像名称
 $s = 'size' . substr($avatar,strpos($avatar, "?s=", $tmp) + 3,2);//图像大小 48
 $w = 'http://images.isouth.org';//博客地址
 $e = ABSPATH .'/images/avatar/'. $f . $s .'.png';//主机图像地址
 $t = 1209600; //14天 单位秒
 if ( !is_file($e) || (time() - filemtime($e)) > $t ) copy($g, $e); //当头像不存在或者图像存在时间超过14天更新
 if ( filesize($e) < 500 ) copy($w.'/avatar/default.png', $e);
 $avatar = strtr($avatar, array($g => $w.'/avatar/' . $f . $s . '.png'));//将原来的地址替换成本地地址
 return $avatar;
}
add_filter('get_avatar', 'cache_gravatar');

/*表情图片地址*/
add_filter('smilies_src','custom_smilies_src',1,10);
function custom_smilies_src ($img_src, $img, $siteurl){
    return 'http://images.isouth.org/emoticons/'.$img;
}
?>
