<?php // Do not delete these lines
	if (isset($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');
	
	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.'); ?></p> 
	<?php
		return;
	}
?>

<?php if ( have_comments() ) : ?>
<?php if ( ! empty($comments_by_type['comment']) ) : ?>
	<h3 id="commentb"><?php printf(__('&#8220;%s&#8221;'), the_title('', '', false)); ?><span id="comments"><?php comments_number(__('快抢沙发！'), __('1条留言'), __('%条留言'));?></span></h3>
	<ol class="commentlist clearfix">
		<?php wp_list_comments('type=comment&callback=mytheme_comment'); ?>
	</ol>
	<div class="comments-nav">
		<?php paginate_comments_links('prev_text=上一页&next_text=下一页');?>
	</div>
<?php endif; ?>
<?php if ( ! empty($comments_by_type['pings']) ) : ?>
    <h3 id="pingback">Pingbacks/Trackbacks</h3>
    <ol class="commentlist clearfix">
	<?php wp_list_comments('type=pings&callback=mytheme_comment'); ?>
	</ol>
<?php endif; ?>
<?php else : ?>
<?php if ('open' == $post->comment_status) : ?>
	<?php else : ?>
	<p class="nocomments"><?php _e('评论关闭了.'); ?></p>
<?php endif; ?>
<?php endif; ?>

<?php if ('open' == $post->comment_status) : ?>
	<div id="respond">
	<h3 class="clearfix"><span style="float:left"><?php comment_form_title( __('发表留言（Ctrl+Enter提交）'), __('给%s个回复' ) ); ?></span> <span id="cancel-comment-reply"><?php cancel_comment_reply_link() ?></span></h3>
	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
	<p><?php printf(__('You must be <a href="%s">logged in</a> to post a comment.'), get_option('siteurl') . '/wp-login.php?redirect_to=' . urlencode(get_permalink())); ?></p>
	<?php else : ?>
	
<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
<?php if ( $user_ID ) : ?>
<p><?php printf(__('Logged in as <a href="%1$s">%2$s</a>.'), get_option('siteurl') . '/wp-admin/profile.php', $user_identity); ?> <a href="http://www.gravatar.com/" rel="external">[设置 Gravatar 头像]</a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account'); ?>"><?php _e('Log out &raquo;'); ?></a></p>
<?php else : ?>

<div class="visitor"><a href="https://en.gravatar.com/" rel="external nofollow" title="设置您的 Gravatar 头像"><?php echo get_avatar( $comment_author_email, $size = '40', 'http://images.isouth.org/qi/guest.gif'); ?></a></div>
<p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1"/>
<label class="small" for="author">昵称 (<span style="color:#CB5C11;">必填</span>)<?php if ($req) _e(" "); ?></label></p>
<p><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2"/>
<label for="email">EMail (不公开，<span style="color:#CB5C11;">必填</span>)<?php if ($req) _e(" "); ?></label></p>
<p><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" />
<label for="url">网站 (<span style="color:#090;">选填</span>)</label></p>
<?php endif; ?>
<?php include(TEMPLATEPATH . '/smiley.php'); ?>
<p><textarea name="comment" id="comment" cols="60" rows="10" tabindex="4" onkeydown="if(event.ctrlKey&amp;&amp;event.keyCode==13){document.getElementById('submit').click();return false};"></textarea></p>
<p><input name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('发布'); ?>" /></p>
<div>
<?php do_action('comment_form', $post->ID); ?>
<?php comment_id_fields(); ?>
</div>
</form>

	<?php endif; // If registration required and not logged in ?>
	</div>
<?php if ( isset($ping_count) && $ping_count ) : ?>
	<div id="trackbacks-list" class="comments">
	<h3><?php printf($ping_count > 1 ? '%d Trackbacks' : '1 Trackback', $ping_count) ?></h3>
	<ol>
	<?php foreach ( $comments as $comment ) : ?>
	<?php if ( get_comment_type() != "comment" ) : ?>
	<li id="comment-<?php comment_ID() ?>">
		<?php comment_author_link(); ?>
		<?php if ($comment->comment_approved == '0') echo('<i>Your trackback is awaiting moderation.</i>\n'); ?>
	</li>
	<?php endif ?>
	<?php endforeach; ?>
	</ol>
	</div>

<?php endif ?>
<?php endif; ?>